#!/usr/bin/env python3

import socket

from game_message import GameMessage
from format import Format



# First room in the game
# Ghost must examine safe and write number on foggy mirror
# Human then opens the safe, gets the key, and leaves to the Corridor room
class Corridor:

    def __init__(self, parent):
        self.parent = parent
        self.add_flags()
        self.add_messages()

    # prints description of room
    def print_description(self):
        description = \
            "You are in a cramped corridor. On one end is a mess of twisted metal and \n" \
            "blue-green fire. On the other is a sealed hatch. On the right are two doors.\n" \
            "On the left is nothing but the room you just came out of.\n" \


        description += Format.COLOR['RED'] + "You can hear growling, but cannot tell which door it is coming from.\n" \
                       + "It would be wise to know if it is safe before opening a door.\n" + Format.END()

        description += "Above each of the two doors is a bell.\n"

        if self.parent.FLAGS['CO_BELL1_RUNG'] == 1 and self.parent.is_host:
            description += Format.COLOR['CYAN']
            description += "The bell above the first door rings.\n"
            description += Format.END()
            self.parent.set_flag('CO_BELL1_RUNG', 0)

        if self.parent.FLAGS['CO_BELL2_RUNG'] == 1 and self.parent.is_host:
            description += Format.COLOR['CYAN']
            description += "The bell above the second door rings.\n"
            description += Format.END()
            self.parent.set_flag('CO_BELL2_RUNG', 0)

        if self.parent.FLAGS['CO_BELL3_RUNG'] == 1 and self.parent.is_host:
            description += Format.COLOR['CYAN']
            description += "The bell above the third door rings.\n"
            description += Format.END()
            self.parent.set_flag('CO_BELL3_RUNG', 0)

        if self.parent.MESSAGES['HUMAN_TALK'] is not None and self.parent.is_host is False:
            description += Format.COLOR['GREEN']
            description += self.parent.other_username + " says something:\n" + self.parent.MESSAGES['HUMAN_TALK'] + '\n'
            description += Format.END()
            self.parent.MESSAGES['HUMAN_TALK'] = None

        #safe room is random, so here is a debug line that shows what room is safe
        #description += "\nDEBUG: Safe door is " + str(self.parent.FLAGS['CO_SAFE_DOOR'])

        print(description)

    # returns a list of action keywords
    # the keywords must match those in the ACTIONS dictionary
    def create_action_list(self):
        # compile actions
        action_list = []
        action_list.append('Look')

        if self.parent.is_host:
            action_list.append('Talk to Ghost')
            action_list.append('Open Hatch')
            action_list.append('Go Through First Door')
            action_list.append('Go Through Second Door')
            action_list.append('Go Through Third Door')

        else:
            action_list.append('Ring First Bell')
            action_list.append('Ring Second Bell')
            action_list.append('Ring Third Bell')
            action_list.append('Look Through First Door')
            action_list.append('Look Through Second Door')
            action_list.append('Look Through Third Door')

        return action_list

    # adds flags to the parent's FLAGS dictionary
    # the values MUST be integers
    def add_flags(self):
        self.parent.FLAGS.update({'CO_BELL1_RUNG': 0})
        self.parent.FLAGS.update({'CO_BELL2_RUNG': 0})
        self.parent.FLAGS.update({'CO_BELL3_RUNG': 0})

    # adds messages to parent's MESSAGES dictionary
    def add_messages(self):
        pass

    # Action handling methods
    # Note: these will be executed by game.py, not the room

    def handle_look(self):
        print("You take another look around to see if anything has changed.")

    def handle_talk(self):
        message = input("Enter what you want to say:\n>>")
        print("You speak to thin air. Hopefully, the ghost of " + self.other_username + " is listening.")
        self.set_message('HUMAN_TALK', message)
        self.unlock_achievement('Can anyone hear me?')

    def handle_open_door(self):
        print("Looking through the porthole, you can see that beyond\n"
              "this hatch is nothing but open sea. The ship must have\n"
              "cracked in two, though how this side still sails is unclear.")

    def handle_ring_bell(self):
        print("Using all of your ghostly strength, you feebly ring the bell. \nHopefully " +
              self.other_username + " will hear it.")
        self.set_flag('CO_BELL1_RUNG', 1)
        self.bells_rung += 1

    def handle_ring_bell2(self):
        print("Using all of your ghostly strength, you feebly ring the bell. \nHopefully " +
              self.other_username + " will hear it.")
        self.set_flag('CO_BELL2_RUNG', 1)
        self.bells_rung += 1

    def handle_ring_bell3(self):
        print("Using all of your ghostly strength, you feebly ring the bell. \nHopefully " +
              self.other_username + " will hear it.")
        self.set_flag('CO_BELL3_RUNG', 1)
        self.bells_rung += 1

    def handle_door1(self):

        print(Format.COLOR['YELLOW'] + "Beyond the door are stairs to the deck. You head up." + Format.END())
        self.move_to_room('Deck')

    def handle_door2(self):
        print(Format.COLOR['RED'] + "Inside the room is a monster! You barely get a look at it\nbefore it tears"
                                        " you apart." + Format.END())
        self.move_to_room('Lose_Room')

    def handle_door3(self):
        print("This room is a dead end, but you find a soggy book on the floor")
        print(Format.FORM['BOLD'] + "Added item: Morse Code Book" + Format.END())
        self.set_flag('MORSE_BOOK', 1)

    def look_door1(self):
        print("You open the first door.")
        print(Format.COLOR['YELLOW'] + "You see stairs leading to the next deck." + Format.END())

    def look_door2(self):
        print("You open the second door.")
        print(Format.COLOR['RED'] + "This room contains a horrible beast!" + Format.END())

    def look_door3(self):
        print("You open the third door.")
        print("This has nothing, except for a book on morse code. It may be useful later.")

        
    # Action keywords
    ACTIONS = {
        'Look': handle_look,
        'Talk to Ghost': handle_talk,
        'Open Hatch': handle_open_door,
        'Ring First Bell': handle_ring_bell,
        'Ring Second Bell': handle_ring_bell2,
        'Ring Third Bell': handle_ring_bell3,
        'Look Through First Door': look_door1,
        'Look Through Second Door': look_door2,
        'Look Through Third Door': look_door3,
        'Go Through First Door': handle_door1,
        'Go Through Second Door': handle_door2,
        'Go Through Third Door': handle_door3
    }
