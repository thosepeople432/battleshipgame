#!/usr/bin/env python3

"""server.py: The server software for the Battleship game.

Limit on TCP connections adapted from https://stackoverflow.com/a/35086035
"""

import socket
import threading
import weakref

from constants import *
from game_message import GameMessage


class PlayerInfo:
    """A collection of information about a connected player which
    includes the player's private IP address, and whether that player
    is hosting a game (waiting for another player).
    """
    def __init__(self, private_ip):
        self.private_ip = private_ip
        self.is_host = False 


class Server:
    """The Battleship game server.

    Server file has to be run first than client file to wait for
    any connection requests from any clients.
    """
    def __init__(self):
        # Server list of registered users
        self.user_list = {}
        # lock for user list
        self.user_list_lock = threading.Lock()
        # current TCP sockets
        self.sockets = weakref.WeakSet()
        # accept clients
        self.handle_clients()

    def debug_log(self, message):
        """Debug logging which only outputs if the DEBUG flag is set.
        """
        if DEBUG:
            print(message)
            
    def num_connections(self):
        """Returns the number of currently running threads."""
        return sum(1 for sock in self.sockets if sock.fileno() >= 0)
        
    def handle_clients(self):
        """Sets up the defined port for listening for TCP connections,
        and creates a thread to handle each incoming connection.
        """
        # Create a TCP socket object
        server_socket = socket.socket()
        # Set the socket to allow reuse of the port
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # Bind the socket to the server port
        server_socket.bind(('', SERVER_LISTEN_PORT));
        # Listen for client connection requests
        server_socket.listen(SERVER_BACKLOG);
        # Accept connection requests from clients
        while True:
            client_socket, client_address = server_socket.accept()
            # if maximum number of threads is already running, close
            # connection
            if self.num_connections() > MAX_TCP_CONNECTIONS:
                print('can\'t accept connection from {}, too many connections'
                      ' already'.format(client_address))
                client_socket.close()
            else:
                print('got connection from {}'.format(client_address))
                # handle each client connection in a separate thread
                self.sockets.add(client_socket)
                thread = ServerThread(self, client_socket)
                thread.start()
                print('started thread for {}'.format(client_address))

    def register_username(self, name, player_info):
        """Registers the given player info into the user list under the
        given name.
        
        Returns True if registration succeeded, otherwise returns
        False.
        """
        with self.user_list_lock:
            # check if name of player already exists, the maximum
            # number of players have already been registered, or
            # the username is too long
            if (name in self.user_list or
                len(self.user_list) >= MAX_USERNAMES or
                len(name) > MAX_USERNAME_LENGTH):
                return False
            else:
                # register new name
                self.user_list[name] = player_info
                self.debug_log('user_list changed, current list:')
                self.debug_log(self.user_list)
                return True

    def unregister_username(self, name, private_ip):
        """Unregisters the given name from the user list.
        
        Returns True if the name was present in the list, otherwise
        returns False.
        """
        with self.user_list_lock:
            # check if username is not registered
            if (name not in self.user_list or
                self.user_list[name].private_ip != private_ip):
                return False
            else:
                # unregister name
                del self.user_list[name]
                self.debug_log('user_list changed, current list:')
                self.debug_log(self.user_list)
                return True

    def get_hosts(self):
        """Returns a list of all players who are currently available
        to play a game.
        """
        with self.user_list_lock:
            return [name for name in self.user_list
                    if self.user_list[name].is_host]

    def create_game(self, host_name):
        """Creates a game with the given host name.

        Returns True if the host is a registered user and no game
        hosted by the given host already exists, otherwise returns
        False.
        """
        with self.user_list_lock:
            if (host_name in self.user_list and
                not self.user_list[host_name].is_host):
                self.user_list[host_name].is_host = True
                self.debug_log('user_list changed, current list:')
                self.debug_log(self.user_list)
                return True
            else:
                return False

    def join_game(self, guest_name, host_name):
        """Joins the guest with the given name to the game hosted by
        the host with the given host name.

        Returns the IP address of the host if both the guest and host
        are registered users, the host is already hosting a game and
        the guest is not, otherwise returns None.
        """
        with self.user_list_lock:
            if (guest_name in self.user_list and
                host_name in self.user_list and
                not self.user_list[guest_name].is_host and
                self.user_list[host_name].is_host):
                # host is no longer hosting game
                self.user_list[host_name].is_host = False
                self.debug_log('user_list changed, current list:')
                self.debug_log(self.user_list)
                # get IP address of host
                return self.user_list[host_name].private_ip
            else:
                return None


class ServerThread(threading.Thread):
    """The thread which handles each client connection."""

    DEBUG = False
    
    def __init__(self, parent, client_socket):
        super(ServerThread, self).__init__()
        self.is_running = True
        self.parent = parent
        self.client_socket = client_socket
        self.client_name = None
        self.client_info = None

    def log(self, message):
        """Prints a message to the output which includes a header
        containing the address data of the current client.
        """
        print('{}: '.format(self.client_socket.getsockname()) + message)

    def debug_log(self, message):
        """Debug logging which only outputs if the DEBUG flag is set.
        """
        if DEBUG:
            self.log(message)

    def close_thread(self):
        """Closes the connection with the client and flags the thread
        to be closed.
        """
        # remove client name and entry from user list
        if self.client_name:
            self.parent.unregister_username(self.client_name,
                                            self.client_info.private_ip)
        self.client_name = None
        self.client_info = None
        self.client_socket.shutdown(socket.SHUT_WR)
        self.client_socket.close()
        self.is_running = False

    def send_message(self, message):
        """Attempts to send the given GameMessage to the client. If
        this fails, it logs an error message, closes the connection
        with the client, and flags the thread to be closed.
        """
        if not message.send(self.client_socket):
            self.log('connection with client was lost')
            self.close_thread()
    
    # Message type handling functions

    def handle_create(self, params):
        """Handles a CREATE message from a particular client."""
        # Do not create game if client is not registered
        if self.client_name is None:
            self.log('client attempted to create game without first'
                     ' registering')
            return
        success = self.parent.create_game(self.client_name)
        if success:
            response = GameMessage('CREATE_OK')
            self.log('created game')
        else:
            response = GameMessage('CREATE_FAIL')
            self.log('create game failed')
        self.send_message(response)
    
    def handle_exit(self, params):
        """Handles a EXIT message from a particular client."""
        self.log('closed connection with client')
        self.close_thread()
    
    def handle_join(self, params):
        """Handles a JOIN message from a particular client.
        
        Expects 1 parameter: host username
        """
        if not params:
            self.log('Error: called handle_join without parameter')
            return
        host_name = params[0]
        # do not join game if client has not registered name
        if self.client_name is None:
            self.log('client attempted to join game without first registering')
            return
        host_address = self.parent.join_game(self.client_name, host_name)
        if host_address:
            response = GameMessage('JOIN_OK', [host_address])
            self.log('joined game hosted by ' + host_name)
        else:
            response = GameMessage('JOIN_FAIL')
            self.log('failed to join game hosted by ' + host_name)
        self.send_message(response)
    
    def handle_list(self, params):
        """Handles a LIST message from a particular client."""
        # get list of all players who are available
        hosts = self.parent.get_hosts()
        # send the hosts list to client
        self.send_message(GameMessage('LIST', hosts))
        self.log('sent list of games')
    
    def handle_register(self, params):
        """Handles a REGISTER message from a particular client.
        
        Expects 2 parameters: username, private IP address
        """
        if not params or len(params) != 2:
            self.log('Error: called handle_register without correct'
                     ' parameters')
            return
        name = params[0]
        client_info = PlayerInfo(params[1])
        # If username registration failed, send NAME_FAIL message,
        # otherwise send NAME_OK
        success = self.parent.register_username(
                name, 
                client_info)
        if success:
            self.client_name = name
            self.client_info = client_info
            response = GameMessage('REGISTER_OK')
            self.log('registered name {} for client'.format(name))
        else:
            response = GameMessage('REGISTER_FAIL')
            self.log('failed to register name {} for client'.format(name))
        self.send_message(response)
    
    def handle_unregister(self, params):
        """Handles a UNREGISTER message from a particular client."""
        success = self.parent.unregister_username(self.client_name,
                                                  self.client_info.private_ip)
        if success:
            self.client_name = None
            response = GameMessage('UNREGISTER_OK')
            self.log('unregistered name')
        else:
            response = GameMessage('UNREGISTER_FAIL')
            self.log('request to unregister name but name didn\'t exist or'
                     ' didn\'t have permission')
        self.send_message(response)
    
    ACCEPTED_MSG_VERBS = {
        'CREATE': handle_create,
        'EXIT': handle_exit,
        'JOIN': handle_join,
        'LIST': handle_list,
        'REGISTER': handle_register,
        'UNREGISTER': handle_unregister
    }

    def run(self):
        """Runs this thread."""
        while self.is_running:
            self.debug_log('accepted message')
            self.accept_message()

    def accept_message(self):
        """Accepts a message from the client and responds
        appropriately.
        """
        message = GameMessage.receive(self.client_socket)
        # If message is None, connection with client was lost
        if message is None:
            self.log('connection with client was lost')
            self.close_thread()
            return
        self.debug_log('verb: ' + message.verb)
        for param in message.params:
            self.debug_log('param: ' + param)
        if message.verb not in ServerThread.ACCEPTED_MSG_VERBS:
            self.log('invalid request received')
        else:
            ServerThread.ACCEPTED_MSG_VERBS[message.verb](self, message.params)
            

if __name__ == '__main__':
    Server()