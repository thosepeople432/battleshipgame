import socket

from constants import *
from game_message import GameMessage
from private_ip import get_ip

def run_test(game_message, server_socket, expected_reply):
    """Runs an individual test of communication with the server."""
    game_message.send(server_socket)
    reply = GameMessage.receive(server_socket)
    if reply.verb == expected_reply:
        print('PASS')
    else:
        print('FAIL')
    
def server_test():
    """Simple tests of server functionality."""
    address = input('Enter IP address: ')
    # create connection to server
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_info = (address, SERVER_LISTEN_PORT)
    server_socket.connect(server_info)
    # Test REGISTER msg for new name
    my_ip = get_ip()
    game_message = GameMessage('REGISTER', ['newname1', my_ip])
    print('registering new name test:')
    run_test(game_message, server_socket, 'REGISTER_OK')
    # REGISTER same name
    print('registering same name test:')
    run_test(game_message, server_socket, 'REGISTER_FAIL')
    # CREATE game with name
    game_message = GameMessage('CREATE')
    print('creating new game test:')
    run_test(game_message, server_socket, 'CREATE_OK')
    # attempt to CREATE additional game
    print('creating same game test:')
    run_test(game_message, server_socket, 'CREATE_FAIL')
    # LIST available games
    print('list of games:')
    msg = GameMessage('LIST', [])
    msg.send(server_socket)
    reply = GameMessage.receive(server_socket)
    for param in reply.params:
        print(param)
    # JOIN own game (not correct, just tests functionality)
    game_message = GameMessage('JOIN', ['newname1'])
    print('joining own game test:')
    run_test(game_message, server_socket, 'JOIN_FAIL')
    # JOIN non-existent game
    game_message = GameMessage('JOIN', ['dummy'])
    print('joining non-existent game test:')
    run_test(game_message, server_socket, 'JOIN_FAIL')
    # UNREGISTER name
    game_message = GameMessage('UNREGISTER')
    print('unregister test:')
    run_test(game_message, server_socket, 'UNREGISTER_OK')
    # EXIT
    print('exiting...')
    msg = GameMessage('EXIT')
    msg.send(server_socket) 
    server_socket.shutdown(socket.SHUT_WR)
    server_socket.close()
    print('done')

if __name__ == '__main__':
    server_test()