import tkinter as tk
import tkinter.messagebox as messagebox

class Lobby:

    def __init__(self):

        # Set up game lobby window
        self.frame = tk.Frame(bg='DodgerBlue4')
        self.frame.pack()
        self.lobby_page(self.frame)
        self.frame.mainloop()


    def lobby_page(self, frame):
        top_frame = tk.Frame(self.frame)
        bottom_frame = tk.Frame(self.frame, bg='DodgerBlue4')
        bottomTop_frame = tk.Frame(bottom_frame)
        bottomBottom_frame = tk.Frame(bottom_frame)
        bottomTopLeft_frame = tk.Frame(bottomTop_frame)
        bottomTopRight_frame = tk.Frame(bottomTop_frame)
        title_label = tk.Label(top_frame, bg='SkyBlue4', font=('Arial', '20'),
                                    width=70, height=1, text='Game Lobby',
                                    relief=tk.RAISED, bd=3)
        title_label.pack()

        game_list = tk.Listbox(bottomTopLeft_frame, bg='light blue', bd=2, width=150,
                               height=30, relief=tk.RAISED)
        game_list.pack(side="left", fill="y")
        scroll_bar = tk.Scrollbar(bottomTopLeft_frame, orient="vertical")
        scroll_bar.config(command=game_list.yview)
        scroll_bar.pack(side="right", fill="y")
        game_list.config(yscrollcommand=scroll_bar.set)

        user_list = tk.Listbox(bottomTopRight_frame, bg='light blue', width=30,
                               height=30)
        user_list.pack(side="left", fill="y")
        scroll_bar2 = tk.Scrollbar(bottomTopRight_frame, orient="vertical")
        scroll_bar2.config(command=user_list.yview)
        scroll_bar2.pack(side="right", fill="y")
        user_list.config(yscrollcommand=scroll_bar.set)


        create_button = tk.Button(bottomBottom_frame, bg='DodgerBlue3', fg='white',
                                  font=('Arial', '10'), command=self.create_game,
                                  text='Create Game')
        create_button.pack(side=tk.BOTTOM)

        top_frame.pack(side=tk.TOP)
        bottom_frame.pack(side=tk.BOTTOM)
        bottomTop_frame.pack(side=tk.TOP)
        bottomBottom_frame.pack(side=tk.BOTTOM)
        bottomTopLeft_frame.pack(side=tk.LEFT)
        bottomTopRight_frame.pack(side=tk.RIGHT)

    def create_game(self):
        #messagebox.showinfo('Notice Message', 'Do you really want to create a new game?')
        result = messagebox.askyesno('Notice Message', 'Do you really want to create a new game?')

        #if result== True:
            #create new Game
        #else:
            #ignore

if __name__ == '__main__':
    Lobby()
