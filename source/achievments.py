#!/usr/bin/env python3

from format import Format

class Achievements:

    #Format:
    #[0] 1 = Unlocked
    #[1] Description
    #[2] 0 = Both, 1 = Human 2 = Ghost

    ACHIEVEMENT_LIST = {
        'Can anyone hear me?': [0, '(Human) Talked to the ghost', 1],
        'Safecracker': [0, 'Opened the safe in the first room', 0],
        'For whom the bell tolls': [0, '(Ghost) Rung 4 bells', 2],
        'Rebirth': [0, 'The human escaped the ship', 0],
        'Partners beyond death': [0, '(Human) Attacked the captain to save the ghost, died trying', 1],
        'Codebreaker': [0, 'The ghost sent a telegram, and the human understood', 0]
    }

    NUM_BELLS = 4

    @staticmethod
    def print_achievements():

        print(Format.FORM['BOLD'] + "\nAchievements Unlocked:" + Format.END())
        #for each achievement in the list
        for key in Achievements.ACHIEVEMENT_LIST:
            # if unlocked, print description
            if Achievements.ACHIEVEMENT_LIST[key][0] == 1:
                output = ''

                #color text
                if Achievements.ACHIEVEMENT_LIST[key][2] == 1:
                    output += Format.COLOR['GREEN']
                elif Achievements.ACHIEVEMENT_LIST[key][2] == 2:
                    output += Format.COLOR['CYAN']
                else:
                    output += Format.COLOR['YELLOW']

                output += key + ": "
                output += Achievements.ACHIEVEMENT_LIST[key][1]
                output += Format.END()

                print(output)


        pause = input("\nPress any key to continue...")

if __name__ == '__main__':
    for key in Achievements.ACHIEVEMENT_LIST:
        Achievements.ACHIEVEMENT_LIST[key][0] = True

    Achievements.print_achievements()


