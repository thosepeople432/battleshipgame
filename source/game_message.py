#!/usr/bin/env python3

"""game_message.py: The message system for network communication in the
Battleship game.
"""

import socket

from constants import *

class GameMessage:
    """A game message, transmitted as a series of text lines separated
    by CRLF newlines, with the verb on the first line, and any number
    of parameters each on their own subsequent lines.
    """
    RECV_BUFSIZE = 1
    
    def __init__(self, verb, params=[]):
        """Initializes the message by receiving the text from the
        connection socket.
        """
        self.verb = verb
        self.params = params


    @staticmethod
    def receive(socket):
        """Returns a GameMessage object constructed from the text sent
        from the connection socket. However, if the client disconnects
        (recv returns 0) or sends garbage data (receives too many
        characters), returns None.
        """
        # read message until a double CRLF appears
        message = ''
        while ('\r\n\r\n' not in message and
               len(message) < MAX_MESSAGE_LENGTH):
            try:
                text = socket.recv(GameMessage.RECV_BUFSIZE)
            except (ConnectionResetError, ConnectionAbortedError):
                return None
            # if text is nothing, client disconnected
            if text == b'':
                return None
            message += text.decode()
        # split message into lines
        lines = message.split('\r\n')
        # get verb and params
        verb = lines[0]
        # params
        params = []
        for line in lines[1:]:
            if line:
                params.append(line)
        return GameMessage(verb, params)


    def send(self, socket):
        """Sends the message to the client over the connection
        socket.

        Returns True if the send succeeded, otherwise returns False.
        """
        # verb
        message = self.verb + '\r\n'
        # params
        for param in self.params:
            message += str(param) + '\r\n'
        # final CR+LF
        message += '\r\n'
        # send
        data = str.encode(message)
        bytes_sent = 0
        while bytes_sent < len(data):
            try:
                sent = socket.send(str.encode(message))
            except (ConnectionResetError, ConnectionAbortedError):
                return False
            # if sent 0 bytes, connection is broken
            if sent == 0:
                return False
            bytes_sent += sent
        return True
