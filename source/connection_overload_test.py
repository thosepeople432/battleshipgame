import socket

from constants import *

def open_connections():
    address = input('IP address of server: ')
    sockets = []
    for x in range(MAX_TCP_CONNECTIONS + 30):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_info = (address, SERVER_LISTEN_PORT)
        server_socket.connect(server_info)
        sockets.append(server_socket)
        
if __name__ == '__main__':
    open_connections()