#!/usr/bin/env python3

import socket

from game_message import GameMessage
from format import Format


# First room in the game
# Ghost must examine safe and write number on foggy mirror
# Human then opens the safe, gets the key, and leaves to the Corridor room
class BroomCloset:

    def __init__(self, parent):
        self.parent = parent
        self.add_flags()
        self.add_messages()

    # prints description of room
    def print_description(self):
        description = \
            "You are in a small metal room. It looks to be used for storage.\n" \
            "You can hear the rough waves as they hit the hull of the ship.\n"

        description += "There is only one way out, but the door is locked and too difficult\n" \
                       "to break down. "

        description += "\n"

        if self.parent.FLAGS['BC_BELL_RUNG'] == 1 and self.parent.is_host:
            description += Format.COLOR['CYAN']
            description += "The bell by the door chimes as it is rung by an unseen force before\n" \
                           "falling silent again. "
            description += Format.END()
            self.parent.set_flag('BC_BELL_RUNG', 0)
        else:
            description += "Next to the door is a small bell. "

        description += "\nIn the corner is a small black and gold floor safe.\n"
        description += "On the wall to your right is a foggy mirror.\n"

        if self.parent.MESSAGES['GHOST_MIRROR'] is not None and self.parent.is_host:
            description += Format.COLOR['CYAN']
            description += "Something is written in the mirror:\n" + self.parent.MESSAGES['GHOST_MIRROR'] + '\n'
            description += Format.END()
            self.parent.MESSAGES['GHOST_MIRROR'] = None

        if self.parent.MESSAGES['HUMAN_TALK'] is not None and self.parent.is_host is False:
            description += Format.COLOR['GREEN']
            description += self.parent.other_username + " says something:\n" + self.parent.MESSAGES['HUMAN_TALK'] + '\n'
            description += Format.END()
            self.parent.MESSAGES['HUMAN_TALK'] = None

        print(description)

    # returns a list of action keywords
    # the keywords must match those in the ACTIONS dictionary
    def create_action_list(self):
        # compile actions
        action_list = []
        action_list.append('Look')

        if self.parent.is_host:
            action_list.append('Talk to Ghost')
            action_list.append('Open Door')
            
            if self.parent.FLAGS['BC_SAFE_EXAMINED'] == 1 and self.parent.FLAGS['BC_SAFE_OPEN'] == 0:
                action_list.append('Enter Safe Combination')

        else:
            action_list.append('Ring Bell')
            action_list.append('Write in Mirror')

        action_list.append('Examine Safe')

        return action_list

    # adds flags to the parent's FLAGS dictionary
    # the values MUST be integers
    def add_flags(self):
        self.parent.FLAGS.update({'BC_BELL_RUNG': 0})
        self.parent.FLAGS.update({'BC_SAFE_OPEN': 0})
        self.parent.FLAGS.update({'BC_SAFE_EXAMINED': 0})

    #adds messages to parent's MESSAGES dictionary
    def add_messages(self):
        self.parent.MESSAGES.update({'GHOST_MIRROR': None})

    # Action handling methods
    # Note: these will be executed by game.py, not the room

    def handle_look(self):
        print("You take another look around, see if anything has changed.")

    def handle_talk(self):
        message = input("Enter what you want to say:\n>>")
        print("You speak to thin air. Hopefully, the ghost of " + self.other_username + " is listening.")
        self.set_message('HUMAN_TALK', message)
        self.unlock_achievement('Can anyone hear me?')

    def handle_open_door(self):
        if self.FLAGS['BC_SAFE_OPEN'] == 1:
            print(Format.COLOR['YELLOW'] + "Using the key from the safe, you unlock the door and step through."
                  + Format.END())
            self.unlock_achievement('Safecracker')
            self.move_to_room('Corridor')

        else:
            print("The door is still locked, and you cannot force it open.")

    def handle_ring_bell(self):
        print("Using all of your ghostly strength, you feebly ring the bell. Hopefully " +
              self.other_username + " will hear it.")
        self.set_flag('BC_BELL_RUNG', 1)
        self.bells_rung += 1

    def handle_examine_safe(self):
        if self.FLAGS['BC_SAFE_OPEN'] == 1:
            if self.is_host:
                print("The safe is open and empty")

            else:
                print(Format.COLOR['GREEN'] + self.other_username +
                      " has opened the safe and taken its contents." + Format.END())

        else:
            if self.is_host and self.FLAGS['BC_SAFE_EXAMINED'] == 0:
                print("The safe is locked. If you had the combination, you could open it.")
                print(Format.FORM['BOLD'] + "Added new action: Enter Safe Combination" + Format.END())
                self.set_flag('BC_SAFE_EXAMINED', 1)

            elif self.is_host and self.FLAGS['BC_SAFE_EXAMINED'] == 1:
                print("The safe is still locked.")

            else:
                print(Format.COLOR['YELLOW'] + "You stick your incorporeal head into the safe.\n"
                                               "Inside is a small golden key and a piece of paper\n"
                                               "with the safe's combination: 1-3-9" + Format.END())

    def handle_safe_combo(self):
        print("The numbers on the safe range from 1 to 22. The combination is 3 numbers long.")

        try:
            safe1 = input("Enter the first number:\n>>")
            safe2 = input("Enter the second number:\n>>")
            safe3 = input("Enter the third number:\n>>")

            safe1 = int(safe1)
            safe2 = int(safe2)
            safe3 = int(safe3)

        except:
            print(Format.COLOR['RED'] + "You cannot turn the dial to something that is not a number." + Format.END())
            return

        if safe1 < 1 or safe1 > 22 or safe2 < 1 or safe2 > 22 or safe3 < 1 or safe3 > 22:
            print(Format.COLOR['RED'] + "The dial can only point between 1 and 22" + Format.END())
            return

        if safe1 == 1 and safe2 == 3 and safe3 == 9:
            print(Format.COLOR['YELLOW'] + "The safe opens with a satisfying click. Inside is a small golden key.\n"
                                           "Perhaps it will open the door." + Format.END())
            self.set_flag('BC_SAFE_OPEN', 1)

        else:
            print(Format.COLOR['RED'] + "That combination does not work." + Format.END())

    def handle_write_mirror(self):
        print("The mirror is foggy enough to write a message in.")
        message = input("Enter what you want to say:\n>> ")
        print("You write your message. Hopefully " + self.other_username + " will notice.")
        self.set_message('GHOST_MIRROR', message)

    # Action keywords
    ACTIONS = {
        'Look': handle_look,
        'Talk to Ghost': handle_talk,
        'Open Door': handle_open_door,
        'Ring Bell': handle_ring_bell,
        'Examine Safe': handle_examine_safe,
        'Enter Safe Combination': handle_safe_combo,
        'Write in Mirror': handle_write_mirror
    }
