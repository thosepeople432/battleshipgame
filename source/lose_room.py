#!/usr/bin/env python3

import socket

from game_message import GameMessage


class LoseRoom:

    def __init__(self, parent):
        self.parent = parent
        self.add_flags()

    # prints description of room
    def print_description(self):
        description = "You lost.\n"

        if self.parent.is_host:
            description += "You watch as your body dies, and you know that you are trapped on this\n" \
                           "godforsaken ship for eternity."

        else:
            description += "As the life leaves " + self.parent.other_username + "'s body, their spirit\n" \
                       "screams. They are now trapped here as well."

        self.parent.thread.stop()

        print(description)

    # returns a list of action keywords
    # the keywords must match those in the ACTIONS dictionary
    def create_action_list(self):
        # compile actions
        action_list = []
        action_list.append('End Game')
        return action_list

    # adds flags to the parent's FLAGS dictionary
    # the values MUST be integers
    def add_flags(self):
        pass

    # Action handling methods
    # Note: these will be executed by game.py, not the room

    def handle_end(self):
        self.FLAGS['GAME_OVER'] = 1

    ACTIONS = {
        'End Game': handle_end
    }