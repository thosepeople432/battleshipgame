#!/usr/bin/env python3

import socket

from game_message import GameMessage
from format import Format


# First room in the game
# Ghost must examine safe and write number on foggy mirror
# Human then opens the safe, gets the key, and leaves to the Corridor room
class Deck:

    def __init__(self, parent):
        self.parent = parent
        self.add_flags()
        self.add_messages()

    # prints description of room
    def print_description(self):
        description = \
                    "You reach the deck of the ship. In the middle of the ship is the captain.\n" \
                    "He sits at a desk covered in old maps and is surrounded by stacks of books.\n" \
                    "The captain speaks:\n"

        if self.parent.FLAGS['DK_NAME_FOUND'] == 0:
            description += Format.COLOR['RED'] + "You should not be here, but since you are...\n" \
                                  "Solve my riddle, tell me my name, and I will let the mortal go.\n" + Format.END()
        else:
            description += Format.COLOR['RED'] + "You have solved my riddle, and I am a demon of my word\n" \
                                             "The mortal may go. There is a boat you can use to escape.\n" \
                                             "The spirit, however, stays here.\n" + Format.END()
        if not self.parent.is_host:
            description += Format.COLOR['CYAN'] + "You know this demon. He is the one who trapped you here:\n" \
                                                  "Davy Jones.\n" + Format.END()

        description += "On the captain's desk is a telegraph that is clicking away.\n"

        if self.parent.FLAGS['DK_LOCKER'] == 1 and self.parent.is_host:
            description += Format.COLOR['CYAN']
            description += "Behind the captain, his metal locker rattles.\n" + \
                           self.parent.other_username + " must be trying to tell you something.\n"
            description += Format.END()
            self.parent.set_flag('DK_LOCKER', 0)

        else:
            description += "Behind the captain is a metal locker.\n"


        if self.parent.MESSAGES['HUMAN_TALK'] is not None and self.parent.is_host is False:
            description += Format.COLOR['GREEN']
            description += self.parent.other_username + " says something:\n" + self.parent.MESSAGES['HUMAN_TALK'] + '\n'
            description += Format.END()
            self.parent.MESSAGES['HUMAN_TALK'] = None

        print(description)

    # returns a list of action keywords
    # the keywords must match those in the ACTIONS dictionary
    def create_action_list(self):
        # compile actions
        action_list = []
        action_list.append('Look')

        if self.parent.is_host:
            action_list.append('Talk to Ghost')
            action_list.append('Listen to Telegraph')

        else:
            action_list.append('Send Telegram')
            action_list.append('Rattle Locker')

        action_list.append('Talk to Captain')

        if self.parent.is_host:
            action_list.append('Tell Captain His Name')
            action_list.append('Attack Captain')
            if self.parent.FLAGS['DK_NAME_FOUND'] == 1:
                action_list.append('Leave Ship and Abandon Spirit')

        return action_list

    # adds flags to the parent's FLAGS dictionary
    # the values MUST be integers
    def add_flags(self):
        self.parent.FLAGS.update({'DK_NAME_FOUND': 0})
        self.parent.FLAGS.update({'DK_LOCKER': 0})
    # adds messages to parent's MESSAGES dictionary
    def add_messages(self):
        self.parent.MESSAGES.update({'DK_TELEGRAPH': None})

    # Action handling methods
    # Note: these will be executed by game.py, not the room

    def handle_look(self):
        print("You take another look around to see if anything has changed.")

    def handle_talk(self):
        message = input("Enter what you want to say:\n>>")
        print("You speak to thin air. Hopefully, the ghost of " + self.other_username + " is listening.")
        self.set_message('HUMAN_TALK', message)
        self.unlock_achievement('Can anyone hear me?')

    def handle_confront(self):
        print("The captain speaks:")

        if self.FLAGS['DK_NAME_FOUND']:
            if self.is_host:
                print(Format.COLOR['RED'] + "You have earned my respect, child. I grant you your freedom,\n"
                                    "but your friend is mine already. Go, and forget what happened here." + Format.END())
            else:
                print(Format.COLOR['RED'] + "You are mine, spirit, now and forever. Your actions today do not change that."
                      + Format.END())

        else:
            if self.is_host:
                print(Format.COLOR['RED'] + "Tell me my name, and I will let you go." + Format.END())
            else:
                print(Format.COLOR['RED'] + "If the mortal tells me their name, they may go.\n"
                                            "But you will never be free." + Format.END())

    def handle_leave_ship(self):
        print(Format.COLOR['YELLOW'] + "You lower yourself off the side in a rowboat and leave the cursed ship,\n"
                                       "abandoning your friend to their fate." + Format.END())
        self.unlock_achievement('Rebirth')
        self.move_to_room('Win_Room')

    def handle_attack(self):
        print("You cannot abandon " + self.other_username + " to their fate. You lunge at the captain")
        print(Format.COLOR['RED'] + "With one fluid motion, he pulls out a knife and slits your throat." + Format.END())
        self.unlock_achievement('Partners beyond death')
        self.move_to_room('Lose_Room')

    def handle_name(self):
        name = input("Tell the captain his name:\n>> ")
        if name == 'Davy Jones':
            print("The captain speaks:")
            print(Format.COLOR['RED'] + "That is my name. You have solved the riddle, and may go." + Format.END())
            print(Format.FORM['BOLD'] + "Added action: Leave Ship and Abandon Spirit" + Format.END())
            self.set_flag('DK_NAME_FOUND', 1)

        else:
            print("The captain speaks:")
            print(Format.COLOR['RED'] + "That is not my name. Try again - we have eternity." + Format.END())

    def handle_telegraph_read(self):

        if self.FLAGS['MORSE_BOOK'] == 1:

            print("You listen to the telegraph. Using your book, you decode the message:")

            if self.MESSAGES['DK_TELEGRAPH'] is None:
                print("The message is gibberish.")
            else:
                print(self.MESSAGES['DK_TELEGRAPH'])
                self.unlock_achievement('Codebreaker')

        else:
            print("You do not know morse code and cannot decipher the message.")

    def handle_telegraph_send(self):
        print("You can send a telegram using this telegraph.")
        message = input("Enter what you want to say:\n>> ")
        print("You send your message. Hopefully " + self.other_username + "\ncan understand morse code.")
        self.set_message('DK_TELEGRAPH', message)

    def handle_locker(self):
        print("You rattle the locker behiind Davy Jones. \nHopefully " +
              self.other_username + " will understand what you mean.")
        self.set_flag('DK_LOCKER', 1)

    # Action keywords
    ACTIONS = {
        'Look': handle_look,
        'Talk to Ghost': handle_talk,
        'Talk to Captain': handle_confront,
        'Leave Ship and Abandon Spirit': handle_leave_ship,
        'Attack Captain': handle_attack,
        'Tell Captain His Name': handle_name,
        'Listen to Telegraph': handle_telegraph_read,
        'Send Telegram': handle_telegraph_send,
        'Rattle Locker': handle_locker
    }
