#!/usr/bin/env python3

import socket

from game_message import GameMessage


class WinRoom:

    def __init__(self, parent):
        self.parent = parent
        self.add_flags()

    # prints description of room
    def print_description(self):
        description = "You win!\n"
        if self.parent.is_host:
            description += "As you row away from the battleship, you see \n" + self.parent.other_username + \
                           " waving on the deck.\nYou have survived, but they are trapped forever."
        else:
            description += "You watch as " + self.parent.other_username + " rows away into the mist.\n" \
                           "You may be trapped for eternity, but they... they will live."

        self.parent.thread.stop()
        print(description)

    # returns a list of action keywords
    # the keywords must match those in the ACTIONS dictionary
    def create_action_list(self):
        # compile actions
        action_list = []
        action_list.append('End Game')
        return action_list

    # adds flags to the parent's FLAGS dictionary
    # the values MUST be integers
    def add_flags(self):
        pass

    # Action handling methods
    # Note: these will be executed by game.py, not the room

    def handle_end(self):
        self.FLAGS['GAME_OVER'] = 1

    ACTIONS = {
        'End Game': handle_end
    }