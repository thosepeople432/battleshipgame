#!/usr/bin/env python3

# YOU MUST RUN Format.start()
# For printing colors
# print Color.[color you want]
# use Color.end to go back to normal
class Format:
    COLOR = {
        'PURPLE': '\033[95m',
        'CYAN': '\033[96m',
        'DARKCYAN': '\033[36m',
        'BLUE': '\033[94m',
        'GREEN': '\033[92m',
        'YELLOW': '\033[93m',
        'RED': '\033[91m',
        'WHITE': '\033[37m'
    }

    FORM = {
        'BOLD': '\033[1m',
        'UNDERLINE': '\033[4m',
        'END': '\033[0m'
    }

    @staticmethod
    def start():
        # check if colorama intalled
        from importlib import util
        spam_spec = util.find_spec('colorama')
        found = spam_spec is not None

        if found:
            from colorama import init
            from colorama import deinit
            init()  # starts colorama

        else:
            for key in Format.COLOR:
                Format.COLOR[key] = ''
            for key in Format.FORM:
                Format.FORM[key] = ''



    @staticmethod
    def END():
        return Format.FORM['END']

    @staticmethod
    def close():
        try:
            deinit()
        except:
            pass


def fix():
    for key in Format.COLOR:
        Format.COLOR[key] = ''

    for key in Format.FORM:
        Format.FORM[key] = ''



if __name__ == '__main__':
    Format.start()
    print(Format.COLOR['CYAN'] + 'Blue' + Format.END())  # Change color to blue
    print(Format.FORM['BOLD'] + 'Bold' + Format.END())  # Change color to blue and bold the text
    print('Nothing')  # Text is back to normal

    message = Format.COLOR['CYAN'] + 'Blue ' + Format.END() + Format.FORM['BOLD'] + 'Bold' + Format.END()
    print(message)
    Format.close()
    i = input('')  # pause it
