#!/usr/bin/env python3

import socket

from game_message import GameMessage

class TestRoom:

    def __init__(self, parent):
        self.parent = parent
        self.add_flags()

    #prints description of room
    def print_description(self):
        description = \
            "You are in a small room. Peeling wallpaper reveal the damp wooden walls behind.\n"
        if self.parent.FLAGS['TR_DOOR_OPEN'] == 1:
            description += "The door is open, and you are free to go through. "
        else:
            description += "There is only one way out, but the door is locked and too difficult\n" \
                           "to break down. "

        description += "\n"

        if self.parent.FLAGS['TR_BELL_RUNG'] == 1 and self.parent.is_host:
            description += "The bell by the door chimes as it is rung by an unseen force before\n" \
                           "falling silent again. "
            self.parent.set_flag('TR_BELL_RUNG', 0)
        else:
            description += "Next to the door is a small bell. "

        description += "\n"

        if self.parent.MESSAGES['HUMAN_TALK'] is not None and self.parent.is_host is False:
            description += self.parent.other_username + " says something:\n" + self.parent.MESSAGES['HUMAN_TALK'] + '\n'
            self.parent.MESSAGES['HUMAN_TALK'] = None

        print(description)

    #returns a list of action keywords
    #the keywords must match those in the ACTIONS dictionary
    def create_action_list(self):
        # compile actions
        action_list = []
        action_list.append('Look')


        if self.parent.is_host:
            action_list.append('Talk to Ghost')
            if self.parent.FLAGS['TR_DOOR_OPEN'] == 0:
                action_list.append('Open Door')
            else:
                action_list.append('Exit Room')

        else:
            action_list.append('Ring Bell')

        return action_list

    #adds flags to the parent's FLAGS dictionary
    #the values MUST be integers
    def add_flags(self):
        self.parent.FLAGS.update({'TR_DOOR_OPEN': 0})
        self.parent.FLAGS.update({'TR_BELL_RUNG': 0})
        
    #Action handling methods
    #Note: these will be executed by game.py, not the room

    def handle_look(self):
        print("You take another look around, see if anything has changed.")

    def handle_talk(self):
        message = input("Enter what you want to say:\n>>")
        print("You speak to thin air. Hopefully, the ghost of " + self.other_username + " is listening.")
        self.set_message('HUMAN_TALK', message)

    def handle_open_door(self):
        print("To your surprise, the door isn't locked and you open it easily.")
        self.set_flag('TR_DOOR_OPEN', 1)

    def handle_exit_room(self):
        print("You leave the room and win the game!\n")
        self.set_flag('GAME_WON', 1)

    def handle_ring_bell(self):
        print("Using all of your ghostly strength, you feebly ring the bell. Hopefully " +
              self.other_username + " will hear it.")
        self.set_flag('TR_BELL_RUNG', 1)

    ACTIONS = {
        'Look': handle_look,
        'Talk to Ghost': handle_talk,
        'Open Door': handle_open_door,
        'Exit Room': handle_exit_room,
        'Ring Bell': handle_ring_bell
    }