#!/usr/bin/env python3

"""client.py: The client software for the Battleship game."""

import socket
import time

from constants import *
from game_message import GameMessage
from game import Game
from private_ip import get_ip


class Client:
    """The Battleship game client.

    The client connects to the server to get the list of available
    games, to create a new game, or to join a preexisting game. Once a
    game has been established, both clients are responsible for mutually
    maintaining the game state.
    """
    def __init__(self):
        """Initialize the connection to the server."""
        try:
            # Private IP address
            self.private_ip = get_ip()
            # Own username
            self.username = ''
            # Socket connection to server
            self.socket = None
            # Run the client
            self.run()
        except:
            print('A fatal error occurred. Closing client.')
        
    def join_server(self, address):
        # Create a TCP socket object
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Connect the socket to the server address(Port & IP address)
        server_info = (address, SERVER_LISTEN_PORT)
        self.socket.connect(server_info)

    def send_message(self, message):
        """Sends a message to the server.
        
        Returns True on success, otherwise returns False.
        """
        if not message.send(self.socket):
            raise Exception
        
    def receive_reply(self):
        """Receives a reply from the server."""
        reply = GameMessage.receive(self.socket)
        if reply is None:
            raise Exception
        else:
            return reply
        
    def register(self):
        """Sends a REGISTER message to the server with the desired
        username and the client's private IP address.
        
        Returns the reply received, or None if receive failed.
        """
        self.send_message(GameMessage('REGISTER',
                          [self.username, self.private_ip]))

    def register_username(self):
        """Get username from input and register username to server."""
        registered = False
        while not registered:
            username = ''
            while username == '':
                username = input('Enter your username: ')
                if username != '' and len(username) <= MAX_USERNAME_LENGTH:
                    self.username = username
                else:
                    print('Invalid username, please try again.')
            self.register()
            reply = self.receive_reply()
            if reply.verb == 'REGISTER_OK':
                print('Registered name ' + self.username)
                return True
            else:
                print('Failed to register that name, choose a different one.')

    def unregister(self):
        self.send_message(GameMessage('UNREGISTER'))
        return self.receive_reply().verb == 'UNREGISTER_OK'
                
    def list(self):
        self.send_message(GameMessage('LIST'))
        return self.receive_reply().params

    def create(self):
        self.send_message(GameMessage('CREATE'))
        return self.receive_reply().verb == 'CREATE_OK'

    def join(self, host_name):
        self.send_message(GameMessage('JOIN', [host_name]))
        reply = self.receive_reply()
        return reply

    def exit(self):
        self.send_message(GameMessage('EXIT'))

    def run(self):
        # Get IP address of server
        address = input('Enter IP address: ')
        # create connection to server
        self.join_server(address)
        # Accept username input
        self.register_username()
        # Turn into the Game Lobby
        lobby = Lobby(self)
        # Check out if the user is host
        if not lobby.is_host:
            host_ip = lobby.host_ip
        else:
            host_ip = self.private_ip
        # Open game
        print('host ip: ' + host_ip)
        g = Game(self.username, lobby.is_host, host_ip)
        # Unregister from and exit server
        self.unregister()
        self.exit()


class Lobby:
    def __init__(self, parent):
        self.parent = parent
        self.host_ip = ''
        self.player_ip = ''
        self.is_host = False
        self.host_name = ''
        self.run()
        

    """Display all available players."""
    def display_players(self):
        players = self.parent.list()
        print('[List of Games]')
        for index, player in enumerate(players):
            print('{}: {}'.format(index, player))

    def create_game(self):
        if self.parent.create():
            self.is_host = True
            self.host_ip = self.parent.private_ip
            self.host_name = self.parent.username
            print('Successfully created game.')
        else:
            print('Failed to create game.')

    def join_game(self, host_name):
        reply = self.parent.join(host_name)
        if reply.verb == 'JOIN_OK':
            self.host_name = host_name
            self.host_ip = reply.params[0]
            self.player_ip = self.parent.private_ip
            self.is_host = False
            print('Successfully joined game.')
        else:
            print('Joining game failed.')
            
    def run(self):
        print('******* Game Lobby *******')
        while True:
            # Display all of games & players
            self.display_players()
            print('Choose one of these options:')
            print('1. Create a game  2. Join a game')
            try:
                num = int(input("option >> "))
            except:
                print('Please enter an integer.')
                continue
                # Create a game
            if num == 1:
                self.create_game()
                break
            elif num == 2:
                host_name = input('Enter the host name: ')
                if host_name in self.parent.list():
                    self.join_game(host_name)
                    break
                else:
                    print('Game with given host name does not exist.')
            else:
                print('Invalid option, try again...')

                
if __name__ == '__main__':
    Client()