#!/usr/bin/env python3

import socket
import threading

from constants import *
from game_message import GameMessage
from format import Format

from broom_closet import BroomCloset
from test_room import TestRoom
from win_room import WinRoom
from corridor import Corridor
from lose_room import LoseRoom
from deck import Deck

from achievments import Achievements


class Game:
    """
    The Game class handles a single game between two players.
    One serves as the host, and opens a socket to the other player.
    The host player is the human, and player 2 is the ghost
    The game will run through all rooms until they are all done, then close the game
    """

    # is_host: if set to true, this game acts as a host and connects to player 2
    # player2_addr: player 2's address. If host, connect to this address. Default to local host
    def __init__(self, player_name = 'Default', is_host = False, host_addr = 'localhost'):
        Format.start()

        print(Format.END() + "Launching game...")

        #PLAYER 2
        if is_host is False:

            #create socket
            print('Connecting to host {}...'.format(host_addr))
            self.conn_socket = self.connect_to_host(host_addr, HOST_LISTEN_PORT)
            print('Connection established. Launching game...\n')


        #HOST
        else:
            #create socket
            print("Waiting for other player...")
            self.conn_socket = self.accept_player2_connection()
            print('Connection established. Launching game...\n')

        self.username = player_name
        self.is_host = is_host

        #initial greet
        gm = GameMessage('GREET', [self.username])
        gm.send(self.conn_socket)
        message = GameMessage.receive(self.conn_socket)
        #Something bad happened, bail
        if message.verb != 'GREET':
            print("Error connecting games. Closing game...")
            return -1
        else:
            self.other_username = message.params[0]

        if self.is_host:
            print("Please welcome " + self.other_username + " to the game!")

        else:
            print("You have connected to " + self.other_username + "'s game.")

        #start thread that accepts game messages
        self.thread = Game_Thread(self, self.conn_socket)
        self.thread.start()

        # Start in first room
        self.cur_room = self.ROOMS['Broom_Closet'](self)

        #local achievemnt-tracking variables
        self.bells_rung = 0

        self.run()


    #runs the game loop
    def run(self):


        #continues loop while game is not won
        while Game.FLAGS['GAME_OVER'] == 0:

            if self.bells_rung == Achievements.NUM_BELLS:
                self.unlock_achievement('For whom the bell tolls')

            #other player has disconnected
            if self.FLAGS['DISCONNECTED'] == 1:
                print('\n' + Format.COLOR['RED'] + self.other_username + " has disconnected." + Format.END())
                Game.FLAGS['GAME_OVER'] = 1


            # Human left the room, ghost needs to catch up
            # change room, then set flag back to 0
            elif self.FLAGS['ROOM_LEFT'] == 1 and self.is_host is False:
                print(Format.COLOR['GREEN'] + self.other_username + " has left the room, and you follow them." +
                      Format.END())
                self.cur_room = self.ROOMS[self.MESSAGES['HUMAN_ROOM']](self)
                self.set_flag('ROOM_LEFT', 0)
                self.set_flag('GHOST_CAUGHT_UP', 1)

            # Human waiting for ghost to catch up
            elif self.FLAGS['GHOST_CAUGHT_UP'] == 0 and self.is_host:
                pass

            #you can display the room
            else:
                self.display_room(self.cur_room)
                pause = input("Press any key to continue...")

        Achievements.print_achievements()

        self.exit_game()

    # displays room description and actions
    def display_room(self, room):
        print() #blank line

        room.print_description()

        action_list = room.create_action_list()

        # prompt for user input
        print(Format.FORM['BOLD'] + "Choose an action:" + Format.END())
        for i in range(len(action_list)):
            print(str(i) + ") " + action_list[i])

        action_ok = True

        a = 0 #to prevent crash
        try:
            a = int(input(">> "))
        except:
            print("Invalid action, try again.")
            action_ok = False

        # if bad input, prompt again
        if a < 0 or a >= len(action_list):
            print("Invalid action, try again.")
            action_ok = False

        # do action
        if action_ok:
            action = action_list[a]
            room.ACTIONS[action](self)

    #human moves to room from the keyword passed in
    def move_to_room(self, room_keyword):

        #room does not exist, return
        if room_keyword not in self.ROOMS:
            print("ERROR: Room " + room_keyword + " does not exist.\n"
                                                  "Blame the programmer.")

        self.set_flag('ROOM_LEFT', 1)
        self.set_flag('GHOST_CAUGHT_UP', 0)
        self.set_message('HUMAN_ROOM', room_keyword)
        self.cur_room = self.ROOMS[room_keyword](self)
        print("Please wait for the ghost to catch up...")

    #creates a TCP socket to a host game
    def connect_to_host(self, address, port):
        # Create a TCP socket object
        conn_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Connect the socket to the server address(Port & IP address)
        p2_info = (address, port)
        conn_socket.connect(p2_info)
        return conn_socket

    #creates a TCP socket from another player
    def accept_player2_connection(self):
        # Create a TCP socket object
        host_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Bind the socket to the port to listen to
        host_socket.bind(('', HOST_LISTEN_PORT))
        # Listen for client connection requests
        host_socket.listen()
        #accept connection
        conn_socket, self.host_address = host_socket.accept()
        return conn_socket

    #end colorama in format, close socket and stop thread to exit gracefully
    def exit_game(self):
        pause = input("Press any key to close game...")

        self.achievements = []

        for key in Achievements.ACHIEVEMENT_LIST:
            if Achievements.ACHIEVEMENT_LIST[key][0] == 1:
                self.achievements.append(key)

        self.conn_socket.shutdown(socket.SHUT_WR)
        self.conn_socket.close()
        self.thread.stop()
        Format.close()



    #sets a flag to the value specified. Also sends it to the other player for updating
    #their game
    #if value not an int, set it to 0
    def set_flag(self, flag, value):
        try:
            value = int(value)
        except ValueError:
            value = 0

        if flag in Game.FLAGS:
            self.FLAGS[flag] = value
            #only send if not disconnected
            if self.FLAGS['DISCONNECTED'] == 0:
                gm = GameMessage('SET_FLAG', [flag,value])
                gm.send(self.conn_socket)

    def unlock_achievement(self, achievement):
        if achievement in Achievements.ACHIEVEMENT_LIST:
            if Achievements.ACHIEVEMENT_LIST[achievement][0] == 0:
                Achievements.ACHIEVEMENT_LIST[achievement][0] = 1
                gm = GameMessage('UNLOCK_ACHIEVEMENT', [achievement])
                gm.send(self.conn_socket)

    # sets a message to the value specified. Also sends it to the other player for updating
    #their game
    def set_message(self, message, value):

        #cut value off if too long
        value = str(value)
        str_end = len(value)
        if str_end > MAX_MESSAGE_LENGTH:
            str_end = MAX_MESSAGE_LENGTH
        value = value[:str_end]

        #if message in messages, set it and sync it
        if message in Game.MESSAGES:
            Game.MESSAGES[message] = value
            gm = GameMessage('SEND_MESSAGE', [message, value])
            gm.send(self.conn_socket)


    #flags must be an int
    #rooms will add their own flags upon initialization
    FLAGS = {
        'GAME_OVER': 0,
        'ROOM_LEFT': 0,
        'GHOST_CAUGHT_UP': 1,
        'DISCONNECTED': 0,
        'MORSE_BOOK': 0
    }

    #Messages are strings
    MESSAGES = {
        'HUMAN_TALK': None,
        'GHOST_TALK': None,
        'HUMAN_ROOM': 'Test_Room' #This is for the ghost player to move to the right room
    }

    #rooms should point to a class with the room methods
    ROOMS = {
        'Test_Room': TestRoom,
        'Win_Room': WinRoom,
        'Lose_Room': LoseRoom,
        'Broom_Closet': BroomCloset,
        'Corridor': Corridor,
        'Deck': Deck
    }


#This thread runs in the background and listens for game messages
#It will handle anything on the ACCEPTED_MSG_VERBS list
class Game_Thread(threading.Thread):

    def __init__(self, parent, conn_socket):
        self.parent = parent
        super(Game_Thread, self).__init__()
        self.conn_socket = conn_socket
        self.running = True

    def run(self):
        """Runs this thread."""

        while self.running:
            try:
                self.accept_message()
                if DEBUG:
                    print('accepted message')

            except ConnectionResetError:
                #stop thread, set disconnected to true
                self.parent.FLAGS['DISCONNECTED'] = 1
                self.stop()
            except ConnectionAbortedError: #seeing if this works
                self.stop()
            except OSError: #seeing if this works
                self.stop()

    def stop(self):
        self.running = False

    def accept_message(self):
        """Accepts a message from the client and responds
        appropriately.
        """

        message = GameMessage.receive(self.conn_socket)

        #if game message is none, then other player has disconnected
        if message is None:
            # stop thread, set disconnected to true
            self.parent.FLAGS['DISCONNECTED'] = 1
            self.stop()
            return


        if DEBUG:
            print('message verb: ' + message.verb)
        for param in message.params:
            if DEBUG:
                print('param: ' + param)
        if message.verb not in Game_Thread.ACCEPTED_MSG_VERBS:
            if DEBUG:
                print('invalid message verb')
            return
        else:
            Game_Thread.ACCEPTED_MSG_VERBS[message.verb](self, message.params)
            if DEBUG:
                print('got correct message verb')

    #gets username from other player
    #now not done in thread, as it was getting it late
    def handle_greet(self, params):
        self.parent.other_username = params[0]
        print("Please welcome " + self.parent.username + " to the game!\n")

    #sets message if in messages
    def handle_send_message(self, params):
        if params[0] in self.parent.MESSAGES:
            self.parent.MESSAGES[params[0]] = params[1]

    #sets flag if it is an actual flag
    #converts to int
    def handle_set_flag(self, params):
        if params[0] in self.parent.FLAGS:
            self.parent.FLAGS[params[0]] = int(params[1])

    def handle_unlock_achievement(self, params):
        if params[0] in Achievements.ACHIEVEMENT_LIST:
            Achievements.ACHIEVEMENT_LIST[params[0]][0] = 1

    #GREET: [0] Sender's username
    #SEND_MESSAGE: [0] Message keyword
    #              [1] Message
    #SET_FLAG: [0] Flag keyword
    #          [1] Flag value (INT)
    #UNLOCK_ACHIEVEMENT: [0] Achievement to unlock
    ACCEPTED_MSG_VERBS = {
        'GREET': handle_greet,
        'SEND_MESSAGE': handle_send_message,
        'SET_FLAG': handle_set_flag,
        'UNLOCK_ACHIEVEMENT': handle_unlock_achievement
    }

#Test - prompt for host/not host and player 2 ip
if __name__ == '__main__':

    i = input("1 = host, 2 = player 2:")

    if i == '2':
        addr = input("Enter host ip (x for localhost):")
        if addr == 'x':
            addr = 'localhost'
        host = False
        name = 'Player 2'

    else:
        host = True
        addr = 'localhost'
        name = 'Host'

    g = Game(player_name = name, is_host = host, host_addr = addr)

    print("Game ended")

    pause = input('')



