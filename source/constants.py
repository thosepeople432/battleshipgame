# Debug flag. If set to true, enables some debug messages.
DEBUG = False

# The listen port used by the server to accept connections from clients.
SERVER_LISTEN_PORT = 12345

# The listen port used by the game host to accept a connection from another player.
HOST_LISTEN_PORT = 8080

# The maximum allowed number of characters in a message.
MAX_MESSAGE_LENGTH = 10000

# The server backlog size.
SERVER_BACKLOG = 25

# The maximum allowed number of concurrent TCP connections in the server.
MAX_TCP_CONNECTIONS = 60

# The maximum allowed amount of registered usernames.
MAX_USERNAMES = 75

# The maximum allowed length of usernames.
MAX_USERNAME_LENGTH = 20