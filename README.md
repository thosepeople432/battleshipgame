# CSS 432 Battleship Game

By Blase Johnson, Hyosang Park, Patrick VanderKnyff

A networked two-player text adventure game written in Python 3.6.

## Requirements:
- Python 3.6 (not guaranteed to work in older Python versions)
- (optional) Colorama: https://github.com/tartley/colorama (Adds color to Windows command prompt)

## Installation / Running:
(game client)
1. Download and install Python 3.6 from https://www.python.org/
2. Make sure that Python is in your PATH environment variable:
	-Windows: https://superuser.com/questions/143119/how-do-i-add-python-to-the-windows-path
3. (optional) Install Colorama: pip install colorama
4. Open up a terminal in the “source” folder, and run: python3 client.py
5.Connect to the ip address of the server: 
	-Our server: 54.245.171.168 

(game server) 
1. Open up a terminal in the “source” folder, and run: python3 server.py

